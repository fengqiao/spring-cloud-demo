package cloud.demo.user.domain.service;

import cloud.demo.user.domain.model.User;

public interface UserService {

	public User getUserById(Integer id);
	
	public Integer addUser(User user);
}
