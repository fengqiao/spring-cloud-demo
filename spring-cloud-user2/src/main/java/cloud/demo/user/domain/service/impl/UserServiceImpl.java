package cloud.demo.user.domain.service.impl;

import org.springframework.stereotype.Service;

import cloud.demo.user.domain.model.User;
import cloud.demo.user.domain.service.UserService;
@Service
public class UserServiceImpl implements UserService{

	@Override
	public User getUserById(Integer id) {
		return new User();
	}

	@Override
	public Integer addUser(User user) {
		System.out.println("add user begin ...");
		System.out.println(user);
		System.out.println("add user end .....");
		return 1;
	}

	
}
