package cloud.demo.offer.interfaces.web;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import cloud.demo.offer.domain.model.meal.Meal;
import cloud.demo.offer.domain.service.MealService;

@RestController
public class MealController {
	@Autowired
    private MealService mealService;

    @RequestMapping("/meal/{id}")
    public Meal getMealById(@PathVariable("id") Integer id){
        return mealService.getOfferById(id);
    }
    
    @GetMapping("/token")
	public String getToken(HttpSession session, HttpServletRequest req) throws JsonProcessingException {
    	Cookie[] cookies = req.getCookies();
    	System.out.println(new com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(cookies));
    	System.out.println("offer session_id = "+session.getId());
		return "offer2 token = "+(String)session.getAttribute("user_token");
	}
}
