package cloud.demo.offer.domain.model.meal;

import java.util.Date;

public class Meal {

	private Integer id;
	private String offerName;
	private Date createDate;
	private Integer operatorId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Integer getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(Integer operatorId) {
		this.operatorId = operatorId;
	}
	
	@Override
	public String toString() {
		return "[id=" + id + ", offerName=" + offerName + ", createDate=" + createDate + ", operatorId="
				+ operatorId + "]";
	}
	
	
}
