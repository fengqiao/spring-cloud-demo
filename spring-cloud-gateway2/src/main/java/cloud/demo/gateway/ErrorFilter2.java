package cloud.demo.gateway;

import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
@Component
public class ErrorFilter2 extends ZuulFilter{

	@Override
	public boolean shouldFilter() {
		return true;
	}

	
	@Override
	public String filterType() {
		return "error";
	}

	@Override
	public int filterOrder() {
		return -1;
	}

	@Override
	public Object run() {
//		try {
//			RequestContext ctx = RequestContext.getCurrentContext();
//			ctx.set("sendErrorFilter.ran", true);
//			ctx.getResponse().getOutputStream().println("{'error':"+ctx.getThrowable()+",'code':"+ctx.getResponseStatusCode()+"}");
//		}
//		catch (Exception ex) {
//			ReflectionUtils.rethrowRuntimeException(ex);
//		}
		return null;
	}
	

}
