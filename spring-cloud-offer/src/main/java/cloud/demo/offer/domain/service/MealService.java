package cloud.demo.offer.domain.service;

import cloud.demo.offer.domain.model.meal.Meal;


public interface MealService {
  
    public Meal getOfferById(Integer id);

    public Integer addOffer(Meal offer);
}
